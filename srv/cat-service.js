const cds = require('@sap/cds');

module.exports = cds.service.impl(async function() {
    const serviceNorth = await cds.connect.to('northwind'); //Nome del modello del package.json
    const serviceDevB = await cds.connect.to('devtoberfest'); //Nome del modello del package.json
    const serviceZpgmj = await cds.connect.to('zpgmj'); //Nome del modello del package.json
   
    this.on('READ', 'Customers', request => {
        return serviceNorth.tx(request).run(request.query);
    });
    this.on('READ', 'Products', request => {
        return serviceDevB.tx(request).run(request.query);
    });
    this.on('READ', 'Libri', request => {
        return serviceZpgmj.tx(request).run(request.query);
    });
   
});
