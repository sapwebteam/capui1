using my.bookshop as my from '../db/data-model';

using {northwind as northwindExt} from './external/northwind.csn';
using {devtoberfest as devtoberfestExt} from './external/devtoberfest.csn';
using {zpgmj as zpgmjExt} from './external/zpgmj.csn';

service CatalogService {
    @readonly entity Books as projection on my.Books;
    @readonly entity Customers as projection on northwindExt.Customers {
        key CustomerID, ContactName
    };
    @readonly entity Products as projection on devtoberfestExt.Products {
        key ID, Name, Description
    };
    @readonly entity Libri as projection on zpgmjExt.LibriSet {
        key Id, Title, Stock
    };

}