# Getting Started

Welcome to your new project.


## Creazione progetto

- Aggiunta in file [mta.yaml](mta.yaml) il servizio 'Connection' che si vuole utilizzare (utilizzare uno già esistente)
- Aggiunta in file [mta.yaml](mta.yaml) il servizio 'xsuaa' che si vuole utilizzare (crearne uno nuovo)
- Importazione metadata dei servizi che andremo ad utilizzare:
    creazione file `xyz.edmx` nella root e all'interno inserire il $metadata del servizio
    Aprire **Terminal** ed eseguire comando `cds import xyz.edmx`
- In [package.json](package.json) sotto l'oggetto cds troveremo la nostra nuova connessione esterna. Aggiungiamo qui il collegamento al nome della nostra destination.
```
    "xyz": {
        "kind": "odata",
        "model": "srv/external/xyz",
        "credentials": {
            "destination": "xyz"
        }
    }
```
- Aggiungere le entita che vogliamo pubblicare sul nostro server sul file [srv/cat-services.cds](srv/cat-services.cds)
- Rimappare la lettura della entità delegando la richiesta al servizio esterno nel file file [srv/cat-services.js](srv/cat-services.js)
```
    const serviceXyz = await cds.connect.to('xyz'); //Nome del modello del package.json
   
    this.on('READ', 'Customers', request => {
        return serviceXyz.tx(request).run(request.query);
    });
```
- Generare file xs-security.json
    Aprire **Terminal** ed eseguire comando `cds compile srv/ --to xsuaa > xs-security.json`

- Generare il file mtar eseguendo tasto destro su [mta.yaml](mta.yaml) -> Build MTA Project
- Deployare l'app sul sistema facendo tasto destro su [mta_archives/capui1_1.0.0.mtar](mta_archives/capui1_1.0.0.mtar) -> Deploy MTA Archive

## Esecuzione locale

- Aprire **Terminal** ed eseguire comando `touch default-env.json` (o creare manualemnte il file `default-env.json` sulla root)
- Andare su SCP -> space -> App e copiare il contenuto delle variabili globali all'interno di questo file
- Creare una run configuration tramite il pulsante play ed eseguirala. 

## Learn More

Learn more at https://cap.cloud.sap/docs/get-started/.
